#!/usr/bin/env python
"""Freeradius test case."""
import logging
import os
import time

import openstack
from openstack.config import loader
from xtesting.core import testcase

import onap_tests.scenario.e2e as e2e
import onap_tests.utils.openstack_utils as openstack_utils

# You must provide your openstack configuration
# in CI we copy the clouds.yaml from .config/openstack
# openstack.enable_logging(True, stream=sys.stdout)
TEST_CLOUD = os.getenv('OS_TEST_CLOUD', 'openlab-vnfs-ci')
loader.OpenStackConfig()

NETWORK_1 = {'name': 'control_plane_net-ci',
             'subnet_name': 'control_plane_subnet-ci',
             'cidr': '193.168.111.0/24'}

NETWORK_2 = {'name': 'user_plane_net-ci',
             'subnet_name': 'user_plane_subnet-ci',
             'cidr': '193.168.112.0/24'}
SHARED_NETWORK = False
NETWORKS = [NETWORK_1, NETWORK_2]


class FreeradiusNbi(testcase.TestCase):
    """Onboard then instantiate a simple Freeradius VNF though ONAP."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init FreeRadius."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'basic_vm'
        super(FreeradiusNbi, self).__init__(**kwargs)
        self.__logger.debug("Freeradius init started")
        self.cloud = openstack.connect(cloud=TEST_CLOUD)
        openstack_utils.create_networks(
            self.cloud, NETWORKS, SHARED_NETWORK)
        self.test = e2e.E2E(service_name='freeradius', nbi=True)
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """Run onap_tests with freeradius VM."""
        self.start_time = time.time()
        self.__logger.debug("start time")
        try:
            self.test.execute()
            self.__logger.info("Freeradius VNF with NBI successfully created")
            # We consider that the cleaning is part of the case
            # not a resouce cleaning step
            self.test.clean()
            self.__logger.info("Freeradius VNF with NBI successfully created")
            self.result = 100
            self.stop_time = time.time()
            return testcase.TestCase.EX_OK
        except Exception:  # pylint: disable=broad-except
            self.result = 0
            self.stop_time = time.time()
            self.__logger.error("Freeradius with NBI test failed.")
            return testcase.TestCase.EX_TESTCASE_FAILED

    def clean(self):
        """Clean VNF resources."""
        openstack_utils.delete_networks(self.cloud, NETWORKS)
