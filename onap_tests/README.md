ONAP Onboarding
===============

Description
-----------

A library to automate some ONAP operations :

- onboarding : vendor, vsp, vf and service
- instantiation : service, CNF/VNF, SDNC preload, VF
- upgrade : vsp

That library is using ONAP SDC API, SO API and AAI API.

Installation
------------

clone the project

if you want to contribute and propose some changes please create a branch and
then begin code modification process.

Go into the cloned project folder (it should be named "onap-tests")

Some information
----------------

the "onap-conf/" folder contains "onap-testing.yaml".
That file is about all ONAP API informations : url, headers, payloads...

You must adapt the last lines to your own environment.

```
openstack:
    cloud_owner: CloudOwner
    region_id: <PUT YOUR REGION ID HERE>
    tenant_name:  <PUT YOUR PROJECT NAME HERE>
    tenant_id: <PUT YOUR PROJECT ID HERE>
```

The "templates/vnf-services/" folder contains a set of files.
Those files contains service related information
that you want to onboard/upgrade/instantiate

If you want to create a model from an existing model. Just copy paste the file
in templates/vnf-services and change the first line of the yaml file.

```
---
<PUT YOUR MODEL NAME>:
tosca_file_from_SDC: service-Ubuntu16-template
version: "1.0"
vnfs:
    - vnf_name: ubuntu16
    .......
```

Prepare your environment
------------------------

you must have
python3
pip3
virtualenv
On windows, "cmder" software is a good solution to obtain a console/terminal

open a console/terminal
go in "onap-tests" folder

create and activate a virtualenv :

Unix OS

```
virtualenv portal
source portal/bin/activate
```

Windows OS:

```
virtualenv portal
portal/Scripts/activate
```

you should be now inside a python virtualenv
and you can install external libraries

```
pip install -r requirements.txt
```

After, you can link the code for the development part:

```shell
python setup.py develop
```

Run an end to end test
----------------------

It will chain onboarding and instantiation (with a sleep for model distribution)

```
python onap_tests/scenario/e2e.py -h
usage: e2e.py [-h] [-s SERVICE] [-c CUSTOMER] [-v VENDOR] [-n]

optional arguments:
  -h, --help            show this help message and exit
  -s SERVICE, --service SERVICE
                        service name: ims, ubuntu16, freeradius, vfw
  -c CUSTOMER, --customer CUSTOMER
                        customer name
  -v VENDOR, --vendor VENDOR
                        vendor name
  -n, --nbi             Use NBI if set
```

If not set, customer will be set to Generic-Vendor and vendor will be set to
generic.

Some examples:

```
python onap_tests/scenario/e2e.py --service ims
python onap_tests/scenario/e2e.py -s ubuntu16 -n
python onap_tests/scenario/e2e.py -s freeradius -n -c new-customer
```

It will first try to onboard the VNF based on the heat and env files located in
the templates/heat_files directory.
For the moment the correspondance between the service name (e.g ims) and the
path of the heat files is indicated in the service configuration file in
templates/vnf-services (e.g. ims-service.yaml) in the parameter
heat_files_to_upload.

At the end of the onboarding, the tosca files will be automatically downloaded
from ONAP and stored in templates/tosca_files (e.g. service-Ims-csar.csar and
service-ims-template.yml).

You can also run the test step by step:

```
import onap_tests.scenario.e2e as e2e
test = e2e.E2E(service_name='ubuntu16')
test.execute()
test.clean()
```

Logs shall be available where you launch the python script.

NBI support
-----------

The service instance can be created natively from the SO or through the NBI
module. If you want to use the NBI API and initiate the instantiation from the
NBI catalog, you need to indicate the NBI option -n.

Run the onboarding from the python console
------------------------------------------

you need a zip file containing heat templates (env+yaml files)
and put that zip file in the /templates/heat_files folder
you need to describe your service
in the /templates/vnf-services/{your_service_name}.yaml files

for example :

```
vFW:
    subscription_type: vFW-service
    tosca_file_from_SDC: service-vFW-Service-template
    vnfs:
        - vnf_name: vPacketGenerator
          heat_files_to_upload: vPacketGenerator.zip
          vnf_parameters: []
        - vnf_name: vFw-vSink
          heat_files_to_upload: vFw-vSink.zip
          vnf_parameters: []
```

Run python to get the interactive python prompt

Then type :

```
import onap_tests.scenario.onboard as onboard
vFW = onboard.Onboard(vendor_name="Generic-Vendor",
                                  service_name="vFW",
                                  customer_name="generic")
vFW.onboard_vendor()
vFW.onboard_vsp()
vFW.onboard_vf()
vFW.onboard_service()
```

That full sequence will create in ONAP SDC :
a vendor, two VSP, two VF and a service.

vendor_name and customer_name are optional:
default values are then used instead :
vendor_name = "generic"
customer_name = "generic"

At the end, the service is distributed.

Use the onboarding library from any python code
-----------------------------------------------

copy the full onap_tests folder in your python library folder

add

```
import onap_tests.scenario.onboard as onboard

```

at the begining of your python code

Instantiate VNFs from the python console
------------------------------------------

Assuming that the service template files are available in the
templates/tosca_files directory and declared in templates/vnf-services, you
can easily instantiate the VNFs through ONAP.
It will simply chain the different operations by cally SO, AAI (to check that
the resources have been created), SDNC (for the preload).
It is similar to what you may find in the robot files from testuite

By default several VNFs can be tested:

- ubuntu16
- ims (clearwater vIMS)
- freeradius
- vfw (canonical example from ONAP community)

You need to perform onboarding first to download service template from SDC.

Run python to get the interactive python prompt

Then type :

```
import onap_tests.scenario.instantiate as instantiate
instantiate_ims = instantiate.Instantiate(service_name="ims")
instantiate_ims.instantiate()
```

You can check on the VID or directly on Openstack that the resources are
properly created.

nbi is an option (True/False), if True the request to the SO are done through
the NBI. The default value is False.

```
instantiate_ims = instantiate.Instantiate(case="ims",nbi=True)
```

You can clean the resources by typing:

```
deploy_ims.clean()
```

Instantiate CNF (over kubernetes environment)
---------------------------------------------

The Instantiate class offers the possibility to instantiate VF modules on
kubernetes(k8s) environment. The condition is to name the VNF using the key "k8s".

The API request towards multiclouds to create the connectivity info object, the
k8s region are not included in onap-tests.

By default, the kubernetes objects are created in "k8s" namespace but it is
possible to specify it indicating namespace_name as parameter.

```
instantiate_k8s = instantiate.Instantiate(service_name="cnfk8s",namespace_name="k8s")
```

Run the upgrade from the python console
---------------------------------------

you need a zip file containing you new heat templates (env+yaml files)
and put that zip file in the /templates/heat_files folder
you need to modify your service description
in the /templates/vnf-services/{your_service_name}-services.yaml files

Run python to get the interactive python prompt

Then type :

```
import onap_tests.scenario.upgrade as upgrade
vFW = upgrade.Upgrade(service_name="vfw")
vFW.upgrade_vsp()
```

At the end, a new VSP version is  Certified in SDC.

Run the tests using xtesting
----------------------------

It is possible to build a docker file to embed the cases (basic_vm,
freeradius_nbi, clearwater_ims and vfw). The Dockerfile can be found in the
docker directory at the root of onap-tests.

You may rebuild your docker by typing:

```
docker build -t <your docker name>:<your docker tag> <home>/docker
```

Once done you can use ii on your system under test. Do not forget to overwritte
some configuration files in order to provide your ids.

```
docker run
-v <your onap-testing.yaml>:/usr/lib/python3.6/site-packages/onap_tests/onap-conf/onap-testing.yaml
-v <your vfw service config>:/usr/lib/python3.6/site-packages/onap_tests/templates/vnf-services/vfw-service.yaml
--add-host portal.api.simpledemo.onap.org:<your ONAP cluster IP>
--add-host sdc.api.fe.simpledemo.onap.org:<your ONAP cluster IP>
--add-host sdc.api.be.simpledemo.onap.org:<your ONAP cluster IP>
--add-host aai.api.sparky.simpledemo.onap.org:<your ONAP cluster IP>
--add-host so.api.simpledemo.onap.org:<your ONAP cluster IP>
--add-host sdnc.api.simpledemo.onap.org:<your ONAP cluster IP>
--add-host nbi.api.simpledemo.onap.org:<your ONAP cluster IP>
--add-host mr.api.simpledemo.onap.org:<your ONAP cluster IP>
<your docker name:your docker tag>
```

Please note that some OpenStack resources (networks, tenant) must be created
before running the use cases.

It is also possible to add your own use case in xtesting:

- create the python class under onap_tests/xtesting
- add the use case in docker/testcases.yaml
- add the entry point in setup.cfg

If you want to leverage the test API and Databalse, do not forget to declare
the use case in the database.

See more details on [xtesting](https://xtesting.readthedocs.io/en/latest/)
