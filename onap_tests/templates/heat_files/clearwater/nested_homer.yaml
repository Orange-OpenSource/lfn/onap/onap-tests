# Project Clearwater - IMS in the Cloud
# Copyright (C) 2015  Metaswitch Networks Ltd
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version, along with the "Special Exception" for use of
# the program along with SSL, set forth below. This program is distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details. You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
# The author can be reached by email at clearwater@metaswitch.com or by
# post at Metaswitch Networks Ltd, 100 Church St, Enfield EN2 6BQ, UK
#
# Special Exception
# Metaswitch Networks Ltd  grants you permission to copy, modify,
# propagate, and distribute a work formed by combining OpenSSL with The
# Software, or a work derivative of such a combination, even if such
# copying, modification, propagation, or distribution would otherwise
# violate the terms of the GPL. You must comply with the GPL in all
# respects for all of the code used other than OpenSSL.
# "OpenSSL" means OpenSSL toolkit software distributed by the OpenSSL
# Project and licensed under the OpenSSL Licenses, or a work based on such
# software and licensed under the OpenSSL Licenses.
# "OpenSSL Licenses" means the OpenSSL License and Original SSLeay License
# under which the OpenSSL Project distributes the OpenSSL toolkit software,
# as those licenses appear in the file LICENSE-OPENSSL.

heat_template_version: 2014-10-16

description: >
  Clearwater Homer node

parameters:
  vnf_name:
    type: string
    label: VNF ID
    description: The VNF name provided by ONAP
  vnf_id:
    type: string
    label: VNF ID
    description: The VNF ID provided by ONAP
  vf_module_id:
    type: string
    label: VNF module ID
    description: The VNF module ID provided by ONAP
  public_net_name:
    type: string
    description: ID of public network
    constraints:
      - custom_constraint: neutron.network
        description: Must be a valid network ID
  homer_name_0:
    type: string
    description: Name of server to use
  homer_flavor_name:
    type: string
    description: Flavor to use
    constraints:
      - custom_constraint: nova.flavor
        description: Must be a valid flavor name
  homer_image_name:
    type: string
    description: Name of image to use
  key_name:
    type: string
    description: Name of keypair to assign
    constraints:
      - custom_constraint: nova.keypair
        description: Must be a valid keypair name
  repo_url:
    type: string
    description: URL for Clearwater repository
  zone:
    type: string
    description: DNS zone
  dns_ip:
    type: string
    description: IP address for DNS server on management network
  dnssec_key:
    type: string
    description: DNSSEC private key (Base64-encoded)
    constraints:
      - allowed_pattern: "[0-9A-Za-z+/=]+"
        description: Must be Base64-encoded
  etcd_ip:
    type: string
    description: IP address of an existing member of the etcd cluster

resources:

  homer_random_str:
    type: OS::Heat::RandomString
    properties:
      length: 4

  homer_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      description: security group
      name:
        str_replace:
          template: pre_base_rand
          params:
            pre: homer_sg_
            base: { get_param: vnf_name }
            rand: { get_resource: homer_random_str }
      rules: [
        {remote_ip_prefix: 0.0.0.0/0, protocol: tcp, port_range_min: 22, port_range_max: 22},
        {remote_ip_prefix: 0.0.0.0/0, protocol: udp, port_range_min: 161, port_range_max: 162},
        {remote_ip_prefix: 0.0.0.0/0, protocol: tcp, port_range_min: 2380, port_range_max: 2380},
        {remote_ip_prefix: 0.0.0.0/0, protocol: tcp, port_range_min: 4000, port_range_max: 4000},
        {remote_ip_prefix: 0.0.0.0/0, protocol: tcp, port_range_min: 7888, port_range_max: 7888},
        {remote_ip_prefix: 0.0.0.0/0, protocol: icmp}]

  homer_0_public_port_0:
    type: OS::Neutron::Port
    properties:
      name:
        str_replace:
          template: pre_base_rand
          params:
            pre: homer_admin_port_0_
            base: { get_param: vnf_name }
            rand: { get_resource: homer_random_str }
      network: { get_param: public_net_name }
      security_groups: [{ get_resource: homer_security_group }]

  homer_server_0:
    type: OS::Nova::Server
    properties:
      name: { get_param: homer_name_0 }
      image: { get_param: homer_image_name }
      flavor: { get_param: homer_flavor_name }
      key_name: { get_param: key_name }
      networks:
        - port: { get_resource: homer_0_public_port_0 }
      metadata: {vnf_id: { get_param: vnf_id }, vf_module_id: { get_param: vf_module_id }, vnf_name: { get_param: vnf_name }}
      user_data_format: RAW
      user_data:
        str_replace:
          params:
            __repo_url__: { get_param: repo_url }
            __zone__: { get_param: zone }
            __dns_ip__: { get_param: dns_ip }
            __dnssec_key__: { get_param: dnssec_key }
            __etcd_ip__ : { get_param: etcd_ip }
            __index__ : 0
          template: |
            #!/bin/bash

            # Log all output to file.
            exec > >(tee -a /var/log/clearwater-heat-homer.log) 2>&1
            set -x

            # Configure the APT software source.
            echo 'deb __repo_url__ ./' | sudo tee --append /etc/apt/sources.list.d/clearwater.list
            apt-get update

            # Get the public IP address from eth0
            sudo apt-get install ipcalc
            ADDR=`ip addr show eth0 | awk '/inet /{print $2}'`
            PUBLIC_ADDR=`ipcalc -n -b $ADDR | awk '/Address:/{print $2}'`

            # Configure /etc/clearwater/local_config.
            mkdir -p /etc/clearwater
            etcd_ip=__etcd_ip__
            [ -n "$etcd_ip" ] || etcd_ip=$PUBLIC_ADDR
            cat > /etc/clearwater/local_config << EOF
            management_local_ip=$PUBLIC_ADDR
            local_ip=$PUBLIC_ADDR
            public_ip=$PUBLIC_ADDR
            public_hostname=homer-__index__.__zone__
            etcd_cluster=$etcd_ip
            EOF

            # Now install the software.
            DEBIAN_FRONTEND=noninteractive apt-get install homer --yes --force-yes
            DEBIAN_FRONTEND=noninteractive apt-get install clearwater-management --yes --force-yes

            # Function to give DNS record type and IP address for specified IP address
            ip2rr() {
              if echo $1 | grep -q -e '[^0-9.]' ; then
                echo AAAA $1
              else
                echo A $1
              fi
            }

            # Update DNS
            retries=0
            while ! { nsupdate -y "__zone__:__dnssec_key__" -v << EOF
            server __dns_ip__
            update add homer-__index__.__zone__. 30 $(ip2rr $PUBLIC_ADDR)
            update add homer.__zone__. 30 $(ip2rr $PUBLIC_ADDR)
            send
            EOF
            } && [ $retries -lt 10 ]
            do
              retries=$((retries + 1))
              echo 'nsupdate failed - retrying (retry '$retries')...'
              sleep 5
            done

            # Use the DNS server.
            # Use the DNS server.
            echo 'nameserver __dns_ip__' > /etc/dnsmasq.resolv.conf
            echo 'RESOLV_CONF=/etc/dnsmasq.resolv.conf' >> /etc/default/dnsmasq
            service dnsmasq force-reload


outputs:
  homer_ip:
    description: IP address in public network
    value: { get_attr: [ homer_server_0, networks, { get_param: public_net_name }, 0 ] }
