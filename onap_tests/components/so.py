#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=no-self-use
#  pylint: disable=too-many-public-methods
"""So class."""

import json
import logging
import time

import requests

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_test_utils
import onap_tests.utils.exceptions as onap_test_exceptions

PROXY = onap_test_utils.get_config("general.proxy")
SO_HEADERS = onap_test_utils.get_config("onap.so.headers")
SO_URL = onap_test_utils.get_config("onap.so.url")
SO_SERVICE_INSTANTIATION_URL = (
    "/onap/so/infra/serviceInstantiation/v7/serviceInstances")
SO_ORCH_REQUEST_URL = "/onap/so/infra/orchestrationRequests/v7/"

class So():
    """Service orchestrator (SO) Class."""

    __logger = logging.getLogger(__name__)

    def __init__(self):
        """Init So class."""
        # Logger
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header=SO_HEADERS,
                       **kwargs):
        trans_id = onap_test_utils.get_uuid()
        header.update(
            {"x-transactionid": trans_id})
        return self.__sender.send_message('SO', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header=SO_HEADERS,
                            **kwargs):
        trans_id = onap_test_utils.get_uuid()
        header.update(
            {"x-transactionid": trans_id})
        return self.__sender.send_message_json('SO', method, action,
                                               url, header, **kwargs)

    @classmethod
    def get_request_info(cls, instance_name):
        """
        Get Request Info.

        :param instance_name: the name of the instance
        """
        return {
            "productFamilyId": instance_name,
            "instanceName": instance_name,
            "source": "VID",
            "suppressRollback": False,
            "requestorId": "test"
        }

    @classmethod
    def get_cloud_config(cls):
        """Get Cloud configuration."""
        return {
            "lcpCloudRegionId": onap_test_utils.get_config(
                "openstack.region_id"),
            "tenantId": onap_test_utils.get_config("openstack.tenant_id"),
            "cloudOwner": onap_test_utils.get_config("openstack.cloud_owner")}

    @classmethod
    def get_subscriber_info(cls, subscriber):
        """
        Get Subscriber Info.

        :param subscriber: the name of the subscriber
        """
        if subscriber == "EMPTY":
            subscriber_id = onap_test_utils.get_config("onap.customer")
        else:
            subscriber_id = subscriber

        return {
            "globalSubscriberId": subscriber_id,
            "subscriberName": subscriber_id}

    @classmethod
    #  pylint: disable=too-many-arguments
    def get_request_param(cls, vnf, alacarte=False, subscription=False,
                          usepreload=False, module_model=None,
                          sdncapi="VNF_API"):
        """
        Get Request parameters.

        :param vnf: the VNF name
        :param alacarte: BPMN used by the SO for the request (True/False)
        :param subscription: SO option (Default: False)
        :param module_model: used only for service and vnf (Default:{})
        :param usepreload: SO option (Default: False)
        :param sdncapi: API used by the SO (VNF_API or GM_API)
        """
        if not module_model:
            # requests to create service and vnf
            request_params = {
                "userParams": [],
                "testApi": sdncapi,
            }
        elif not "k8s" in module_model["modelName"]:
            # requests to create vfmodule (type VNF)
            request_params = {
                "userParams": [
                    {
                        "name": "template_type",
                        "value": "heat"
                    }],
                "testApi": sdncapi,
            }
        else:
            # requests to create vfmodule (type CNF)
            # but k8s key to be present in VNF name
            service_name = onap_test_utils.get_config("onap.service.name")
            request_params = {
                "userParams": [
                    {
                        "name": "definition-name",
                        "value": module_model["modelName"]
                    },
                    {
                        "name": "definition-version",
                        "value": module_model["modelVersion"]
                    },
                    {
                        "name": "profile-name",
                        "value": service_name
                    },
                    {
                        "name": "template_type",
                        "value": "heat"
                    }],
                "testApi": sdncapi,
            }

        if alacarte:
            request_params.update(
                {"aLaCarte": alacarte})

        if usepreload:
            request_params.update(
                {"usePreload": True})

        if subscription:
            request_params.update(
                {"subscriptionServiceType": vnf})

        return request_params

    @classmethod
    def get_service_model_info(cls, invariant_uuid, uuid,
                               service_model_version="1.0"):
        """Return VNF model info."""
        return {
            "modelType": "service",
            "modelName": "test-service",
            "modelInvariantId": invariant_uuid,
            "modelVersion": service_model_version,
            "modelNameVersionId": uuid,
            "modelVersionId": uuid,
        }

    @classmethod
    # pylint: disable=too-many-arguments
    def get_vnf_model_info(cls, vnf_invariant_id, vnf_version_id, vnf_version,
                           vnf_model_name, vnf_customization_id,
                           vnf_customization_name):
        """Get VNF model info."""
        return {
            "modelType": "vnf",
            "modelInvariantId": vnf_invariant_id,
            "modelVersionId": vnf_version_id,
            "modelName": vnf_model_name,
            "modelCustomizationId": vnf_customization_id,
            "modelCustomizationName": vnf_customization_name,
            "modelVersion": vnf_version
        }

    @classmethod
    # pylint: disable=too-many-arguments
    def get_net_model_info(cls, net_invariant_id, net_version_id, net_version,
                           net_model_name, net_customization_id,
                           net_customization_name):
        """Get NET model info."""
        return {
            "modelType": "network",
            "modelInvariantId": net_invariant_id,
            "modelVersionId": net_version_id,
            "modelName": net_model_name,
            "modelCustomizationId": net_customization_id,
            "modelCustomizationName": net_customization_name,
            "modelVersion": net_version
        }

    def get_vnf_related_instance(self, instance_id, invariant_uuid, uuid,
                                 service_model_version):
        """Get VNF related instance."""
        return {
            "instanceId": instance_id,
            "modelInfo": self.get_service_model_info(invariant_uuid,
                                                     uuid,
                                                     service_model_version)
        }

    @classmethod
    #  pylint: disable=too-many-arguments
    def get_module_model_info(cls, module_invariant_id,
                              module_name, module_customization_id,
                              module_version_id, module_version):
        """Get Module model info."""
        return {
            "modelType": "vfModule",
            "modelInvariantId": module_invariant_id,
            "modelCustomizationName": module_name,
            "modelName": module_name,
            "modelVersion": module_version,
            "modelCustomizationId": module_customization_id,
            "modelVersionId": module_version_id}

    #  pylint: disable=too-many-arguments
    def get_module_related_instance(self, vnf_id, vnf_invariant_id,
                                    vnf_version_id, vnf_version,
                                    vnf_model_name, vnf_custom_id,
                                    vnf_custom_name):
        """Get module related Instance."""
        return {
            "instanceId": vnf_id,
            "modelInfo": self.get_vnf_model_info(vnf_invariant_id,
                                                 vnf_version_id,
                                                 vnf_version,
                                                 vnf_model_name,
                                                 vnf_custom_id,
                                                 vnf_custom_name)}
# ---------------------------------------------------------------------
# Payloads
# ---------------------------------------------------------------------

    def get_service_payload(self, vnf, request_info,
                            model_info, customer_info):
        """Get SO Service paylod."""
        return json.dumps({
            "requestDetails": {
                "requestInfo": request_info,
                "modelInfo": model_info,
                "requestParameters": self.get_request_param(vnf, True, True),
                "cloudConfiguration": self.get_cloud_config(),
                "owningEntity": {
                    "owningEntityId": "6b5b6b70-4e9a-4f6f-8b7b-cbd7cf990c6e",
                    "owningEntityName": "OE-generic"},
                "platform": {
                    "platformName": "Platform-name"},
                "lineOfBusiness": {
                    "lineOfBusinessName": "Line-of-business-name"},
                "subscriberInfo": self.get_subscriber_info(customer_info)}})

    def get_vnf_payload(self, vnf, request_info, vnf_model_info,
                        vnf_related_instance):
        """Get SO VNF payload."""
        return json.dumps({
            "requestDetails": {
                "requestInfo": request_info,
                "modelInfo": vnf_model_info,
                "platform": {
                    "platformName": "Platform-name"},
                "requestParameters": self.get_request_param(vnf, True, True),
                "relatedInstanceList": [{
                    "relatedInstance": vnf_related_instance
                }],
                "cloudConfiguration": self.get_cloud_config()}
        })

    def get_module_payload(self, vnf, request_info, module_model_info,
                           vnf_related_instance,
                           module_related_instance):
        """Get SO Module Instance payload."""
        return json.dumps({
            "requestDetails": {
                "requestInfo": request_info,
                "modelInfo": module_model_info,
                "requestParameters": self.get_request_param(vnf, False, False,
                                                            True,
                                                            module_model_info),
                "relatedInstanceList": [
                    {"relatedInstance": vnf_related_instance},
                    {"relatedInstance": module_related_instance}],
                "cloudConfiguration": self.get_cloud_config()}
        })

    def create_instance(self, so_service_payload):
        """
        SO create instance.

        :param so_service_payload: the compelte payload for so request
        """
        url = SO_URL + SO_SERVICE_INSTANTIATION_URL
        self.__logger.debug("SO request: %s", url)
        response = self.__send_message('POST', 'create service instance',
                                       url, data=so_service_payload)
        self.__logger.info("SO create service request: %s",
                           response.text)
        so_instance_id_response = response.json()
        self.__logger.debug("so_instance_id_response: %s",
                            so_instance_id_response)
        instance_id = (
            so_instance_id_response['requestReferences']['instanceId'])
        return instance_id

    def create_vnf(self, instance_id, so_vnf_json_payload):
        """
        Creation of a vnf.

        :param instance_id: the instance id
        :param so_vnf_json_payload: the so payload
        """
        url = (SO_URL + SO_SERVICE_INSTANTIATION_URL +
               "/" + instance_id + "/vnfs")
        self.__logger.debug("SO request: %s", url)
        response = self.__send_message('POST', 'create vnf', url,
                                       data=so_vnf_json_payload)
        vnf_id_response = response.json()
        self.__logger.debug("SO create VNF response %s", response.text)
        vnf_id = vnf_id_response['requestReferences']['instanceId']
        return vnf_id

    def create_net(self, instance_id, so_net_json_payload):
        """
        Creation of a network.

        :param instance_id: the instance id
        :param so_vnf_json_payload: the so network payload
        """
        url = (SO_URL + SO_SERVICE_INSTANTIATION_URL +
               "/" + instance_id + "/networks")
        self.__logger.debug("SO request: %s", url)
        header = SO_HEADERS
        trans_id = onap_test_utils.get_uuid()
        header.update(
            {"x-transactionid": trans_id})
        response = requests.post(url, headers=header,
                                 proxies=PROXY, verify=False,
                                 data=so_net_json_payload)
        net_id_response = response.json()
        self.__logger.debug("SO create Network response %s", response.text)
        return net_id_response

    def create_module(self, instance_id, vnf_id, so_module_payload):
        """Creation of the SO module instance."""
        url = (SO_URL + SO_SERVICE_INSTANTIATION_URL + "/" +
               instance_id + "/vnfs/" + vnf_id + "/vfModules")
        self.__logger.info("SO create module request: %s", url)
        response = self.__send_message('POST', 'create module', url,
                                       data=so_module_payload)
        module_id = response.json()
        return module_id

    def delete_instance(self, instance_id, so_service_json_payload):
        """Delete SO instance."""
        delete_instance = False
        self.__logger.info("Delete instance %s", instance_id)
        try:
            url = (SO_URL + SO_SERVICE_INSTANTIATION_URL + "/" +
                   instance_id)
            self.__logger.debug("SO request: %s", url)
            response = self.__send_message('DELETE', 'delete instance', url,
                                           data=so_service_json_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Instance ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the instance %s",
                                instance_id)
        return delete_instance

    def delete_vnf(self, instance_id, vnf_id, so_vnf_payload):
        """Delete vnf instance."""
        delete_instance = False
        self.__logger.info("Delete vnf %s", vnf_id)
        try:
            url = (SO_URL + SO_SERVICE_INSTANTIATION_URL + "/" +
                   instance_id + "/vnfs/" + vnf_id)
            self.__logger.debug("SO request: %s", url)
            response = self.__send_message('DELETE', 'delete vnf', url,
                                           data=so_vnf_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Vnf ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the vnf %s", vnf_id)
        return delete_instance

    def delete_net(self, instance_id, net_id, so_net_payload):
        """Delete network instance."""
        delete_instance = False
        try:
            url = (SO_URL + SO_SERVICE_INSTANTIATION_URL + "/" +
                   instance_id + "/networks/" + net_id)

            self.__logger.debug("SO request: %s", url)
            response = self.__send_message('DELETE', 'delete network', url,
                                           data=so_net_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Net ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the net %s", net_id)
        return delete_instance

    def delete_module(self, so_module_payload, instance_id, vnf_id, module_id):
        """Delete module instance."""
        delete_instance = False
        try:
            url = (SO_URL + SO_SERVICE_INSTANTIATION_URL + "/" +
                   instance_id + "/vnfs/" + vnf_id + "/vfModules/" + module_id)

            self.__logger.debug("SO request: %s", url)
            response = self.__send_message('DELETE', 'delete module', url,
                                           data=so_module_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Vnf ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the vnf %s", vnf_id)
        return delete_instance

    def get_request_logs(self, request_id=""):
        """
        Get Info on a SO request.

        :param request_id: the request id
        """
        url = SO_URL + SO_ORCH_REQUEST_URL + request_id
        self.__logger.debug("SO request: %s", url)
        return self.__send_message_json('GET', 'get logs', url)

    def check_so_request_completed(self, request_id):
        """
        Check the request status in the SO.

        :param request_id: the request id
        """
        url = SO_URL + SO_ORCH_REQUEST_URL + request_id
        request_completed = False
        response = ""
        nb_try = 0
        nb_try_max = 30
        while request_completed is False and nb_try < nb_try_max:
            response = self.__send_message_json('GET', 'get requests', url)
            self.__logger.info("SO: looking for %s request status....",
                               request_id)
            self.__logger.debug("SO answer: %s", response)
            status = response['request']['requestStatus']['requestState']
            self.__logger.info("SO: response status %s", status)
            if "complete" in status.lower():
                self.__logger.info("Request completed")
                request_completed = True
            if "failed" in status.lower():
                raise onap_test_exceptions.SoCompletionException
            time.sleep(10)
            nb_try += 1

        if request_completed is False:
            self.__logger.info("Request not seen completed")
            raise onap_test_exceptions.SoRequestException

        return (request_completed, response)
